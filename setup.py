from setuptools import setup, find_packages

setup(
    name="ng-gem-challenge",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
)
