# Engine GEM SPaaS Coding Challenge

Implementation of the [Engie GEM coding challenge](https://github.com/gem-spaas/powerplant-coding-challenge).

Time spent: ~3 hours

## Details on the implementation

The unit-commitment algorithm is implemented in the `ng.uc` module, the API endpoint
in the `ng.views` module. The algorithm will try to use first the cheapest powerplants
at their maximum.

*Remark 1*: from the payloads provided, the third one cannot map the requested load even when
all powerplants are maxed out.

*Remark 2*: The `ng.uc` module also contains a linear-programming approach to the problem -
using `scipy.optimize` solvers (this algo is not used with the API).

# Requirements

- Python 3.8
- Docker 19.03
- docker-compose 1.25

Install python dependencies for the API:

```bash
pip install -r requirements.txt
```
# How to run

## Using Django manage.py

You can run the API directly using Python, or using Docker/docker-compose:


```bash
# Install the ng library, that includes the API (Django-based)
pip install .
python manage.py runserver 0.0.0.0:8888
```

You can hit **http://localhost:8888**

```
curl -v -H "Content-Type: application/json" -X POST http://localhost:8888/productionplan -d @payloads/payload1.json
```

## Using docker (or compose)


```
docker-compose build
docker-compose up
```
 
# How to test

```bash
pip install -r requirements-testing.txt
pip install .
pytest .
```
