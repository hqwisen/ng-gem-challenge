FROM python:3.8

WORKDIR /ng

COPY . .

RUN \
    # install requirements
    pip install -r requirements.txt && \
    # install the web api
    pip install .

CMD ["python", "manage.py", "runserver", "0.0.0.0:8888"]
