import json

from dacite import from_dict
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .uc import unit_commitment, Payload


@csrf_exempt
def productionplan(request):
    if request.method != "POST":
        # Method Not Allowed
        return HttpResponse(status=405)
    elif request.content_type == "application/json":
        if len(request.body) == 0:
            return JsonResponse({"error": "no payload provided"}, status=400)
        payload = from_dict(data_class=Payload, data=json.loads(request.body))
        response = unit_commitment(payload)
        # We return a list, which is non-dict hence the safe=False
        return JsonResponse(response, safe=False)
    else:
        # Bad Request
        return HttpResponse(status=400)
