import operator
from dataclasses import dataclass
from functools import partial
from typing import List

import math
from scipy.optimize import minimize

# map to get the fuel key based on the powerplant type
FUEL_TYPE_MAP = {
    "gasfired": "gas(euro/MWh)",
    "turbojet": "kerosine(euro/MWh)",
    "windturbine": "wind(%)",
}

# map to get the powerplant type based on the fuel key
TYPE_FUEL_MAP = {FUEL_TYPE_MAP[power_type]: power_type for power_type in FUEL_TYPE_MAP}


@dataclass
class Powerplant:
    name: str
    type: str
    efficiency: float
    pmin: int
    pmax: int


@dataclass
class Payload:
    load: int
    fuels: dict
    powerplants: List[Powerplant]


def calc_load(powerplants: List[Powerplant], p):
    return round(sum([powerplants[i].efficiency * p[i] for i in range(len(p))]))


def fuel_cost(powerplants: List[Powerplant], fuels: dict, p):
    """
    Fuel cost function (to minimize!).
    :param powerplants: powerplants to use
    :param fuels: fuels costs
    :param p: vector p, where p[i] = power produced for powerplant i
    :return: objective function value sum_i (e_i * p_i)
    """
    return sum(
        [fuels[FUEL_TYPE_MAP[powerplants[i].type]] * p[i] for i in range(len(p))]
    )


def exact_load(powerplants: List[Powerplant], load: int, p):
    """
    Exact load constraint
    :param powerplants: powerplants to use
    :param load: load to generate
    :param p: vector p, where p[i] = power produced for powerplant i
    :return: 0 if vector p produces the exact load
    """
    generated_load = calc_load(powerplants, p)
    # geneated_load = load,
    # that is the sum of produced power must be equal to the requested load
    # note: the constraints is of type eq that is: constraint = 0,
    # so we move the load on the left side
    return generated_load - load


def _unit_commitment_lin(payload: Payload) -> List[dict]:
    """
    CHALLENGE NOTE: This is a linear-programming solution, which apparently is not the requested
    way for the challenge. See unit_commitment function below for a tentative implementation
    of the algorithm.


    Unit-commitment problem resolver implemented as a linear program to minimize the fuel cost.
    :param payload: request payload containing powerplants, fuels and load data
    :return: power to produce per powerplant to generate the load
    """
    # p0 = all powerplant at their min!
    # We try to minimize from this init state, when all powerplants are shutdown.
    p0 = [powerplant.pmin for powerplant in payload.powerplants]
    load, powerplants, fuels = payload.load, payload.powerplants, payload.fuels
    # bounds: pmin <= p <= pmax
    bounds = [(powerplant.pmin, powerplant.pmax) for powerplant in powerplants]
    constraints = [
        # constraint: sum_{i}^{n}  (e_i p_i) = load <=> generated_load = load
        {
            "type": "eq",
            "fun": partial(exact_load, powerplants, load),
        }
    ]
    # Objective is to minimize fuel cost
    res = minimize(
        partial(fuel_cost, powerplants, fuels),
        p0,
        # method='SLSQP',
        constraints=constraints,
        # bounds=bounds,
    )
    # optimized vector
    p = list(map(round, res.x))
    # NOTE non-solution scenario is not handled here
    return [{"name": powerplants[i].name, "p": p[i]} for i in range(len(powerplants))]


# non-lin prog version

def _power_plants_form_fuel_type(
    fuel_type, powerplants: List[Powerplant]
) -> List[Powerplant]:
    return [
        powerplant
        for powerplant in powerplants
        if powerplant.type == TYPE_FUEL_MAP[fuel_type]
    ]


def _unit_commitment_merit(payload: Payload) -> List[dict]:
    """
    Idea is to maxed out the cheapest, and then take the additional load with the expensive ones.
    :return: the response in the format List[dict], where dict is {name: powerplant, p: p}.
    """
    load, powerplants, fuels = payload.load, payload.powerplants, payload.fuels
    # get the cheapest fuel type
    min(fuels.items(), key=operator.itemgetter(1))
    # we don't take CO2 into account to generate the power (yet)
    del fuels["co2(euro/ton)"]
    # reorder based on cost (chepeast = first)
    fuels = {
        type: cost for type, cost in sorted(fuels.items(), key=lambda item: item[1])
    }
    current_load = 0
    response = []
    # browse from the cheapest fuel type to the most expensive
    for fuel_type in fuels:
        for powerplant in _power_plants_form_fuel_type(fuel_type, powerplants):
            remaining_load = load - current_load
            # If the powerplant can fullfill the remaining load, then it does
            # since the cheapest one already took some part of the load before
            mingen = round(powerplant.efficiency * powerplant.pmin)
            maxgen = round(powerplant.efficiency * powerplant.pmax)
            if mingen <= remaining_load <= maxgen:
                response.append(
                    {
                        "name": powerplant.name,
                        "p": math.floor(remaining_load / powerplant.efficiency),
                    }
                )
                current_load += remaining_load
            # If even maxed out there is still some load .. then we use the cheapest one at its max
            elif remaining_load > maxgen:
                current_load += maxgen
                response.append(
                    {
                        "name": powerplant.name,
                        "p": math.floor(maxgen / powerplant.efficiency),
                    }
                )
            else:
                response.append({"name": powerplant.name, "p": 0})
    return response


# default unit-commitment algo (used in the api)
unit_commitment = _unit_commitment_merit

if __name__ == "__main__":
    # test main - outside the api
    import json
    from dacite import from_dict

    with open("payloads/payload3.json") as f:
        payload = json.load(f)
    payload = from_dict(data_class=Payload, data=payload)
    print(unit_commitment(payload))
